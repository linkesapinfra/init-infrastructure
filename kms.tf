resource "aws_kms_key" "ebs-key" {
  description = "KMS Key for EBS volumes"
  key_usage = "ENCRYPT_DECRYPT"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  lifecycle {
    prevent_destroy = true
  }
  tags = merge(
    var.global_tags, tomap({"name" = format("%s-ebs", module.prefix.key_pair)})
  )
}
resource "aws_kms_alias" "ebs-key-alias" {
  name          = "alias/CMK-KMS-EBS"
  target_key_id = aws_kms_key.ebs-key.key_id
}

resource "aws_kms_key" "efs-key" {
  description = "KMS Key for EFS volumes"
  key_usage = "ENCRYPT_DECRYPT"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  lifecycle {
    prevent_destroy = true
  }
  tags = merge(
    var.global_tags, tomap({"name" = format("%s-efs", module.prefix.key_pair)})
  )
}
resource "aws_kms_alias" "efs-key-alias" {
  name          = "alias/CMK-KMS-EFS"
  target_key_id = aws_kms_key.efs-key.key_id
}

resource "aws_kms_key" "s3-key" {
  description = "KMS Key for S3 buckets"
  key_usage = "ENCRYPT_DECRYPT"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  lifecycle {
    prevent_destroy = true
  }
  tags = merge(
    var.global_tags, tomap({"name" = format("%s-s3", module.prefix.key_pair)})
  )
}
resource "aws_kms_alias" "s3-key-alias" {
  name          = "alias/CMK-KMS-S3"
  target_key_id = aws_kms_key.s3-key.key_id
}

resource "aws_kms_key" "sm-key" {
  description = "KMS Key for Secrets Manager"
  key_usage = "ENCRYPT_DECRYPT"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  lifecycle {
    prevent_destroy = true
  }
  tags = merge(
    var.global_tags, tomap({"name" = format("%s-secrets", module.prefix.key_pair)})
  )
}
resource "aws_kms_alias" "sm-key-alias" {
  name          = "alias/CMK-KMS-SM"
  target_key_id = aws_kms_key.sm-key.key_id
}
