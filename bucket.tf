resource "aws_s3_bucket" "bck_ansible" {
  bucket      = format("ansible-ssm-%s", data.aws_caller_identity.current.account_id)
  acl         = "private"
  lifecycle {
    prevent_destroy = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.s3-key.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }
  tags = merge(
    var.global_tags, tomap({ "Name" = format("%s-Ansible", module.prefix.bucket)})
    )
}

resource "aws_s3_bucket" "bck_tfstate" {
  bucket      = format("tfstate-%s", data.aws_caller_identity.current.account_id)
  acl         = "private"
  lifecycle {
    prevent_destroy = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.s3-key.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }
  tags = merge(
    var.global_tags, tomap({ "Name" = format("%s-tfstate", module.prefix.bucket)})
    )
}
