variable "project_code" {
  type = string
  default = "lnk-demo"
  description = "Project code"
}

variable "global_tags" {
  type = map(string)
  default = {
    "Owner"    = "salvador.alavedra@syntax.com"
    "Deployer" = "LinkeIT"
    "Project"  = "sap-ansible"
  }
}
variable "region" {
  type        = string
  default     = "eu-west-1"
}
