resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name           = format("lock-table-%s", data.aws_caller_identity.current.account_id)
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = merge(
    var.global_tags, tomap({
      "Name" = format("lock-table-%s", data.aws_caller_identity.current.account_id)
      })
  )
  lifecycle {
    prevent_destroy = true
  }
}
