# Initializing infraestructure for deploy SAP elements

This code is an example of the how to deploy the minimal infraestructure for to work with the code writed in terrafrom and ansible, and deploy a complete SAP infrastructure.
The execution of this code is the first step and must deployed 24h before of launch the ansibles

### What are we deploying?

* 2 S3 Buckets
  - ansible-ssm-<AWS acoound ID>
  - tfstate-<AWS acoound ID>
* 3 KMS CMK keys
  - KMS Key for EBS volumes
  - KMS Key for EFS
  - KMS Key for S3 buckets
* Dynamo DB table: for the terraform locks

## Usage

Only need to clone and duplicate this code an change the values in variables.tf

# ATENTION! #
Due an issue with the SSM plugin for AWS, you must deploy this infra 24 hours before to launch the ansible codes. This is due to in this code we create an encripted bucket named "ansible-ssm-<AWS acoound ID>" and this bucket needs to be created with 24 hours of time "https://github.com/ansible-collections/community.aws/issues/127"

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_prefix"></a> [prefix](#module\_prefix) | git@bitbucket.org:linkesapinfra/naming-sap.git | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table.dynamodb-terraform-state-lock](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_kms_alias.ebs-key-alias](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_alias.efs-key-alias](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_alias.s3-key-alias](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_alias.sm-key-alias](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.ebs-key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key.efs-key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key.s3-key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key.sm-key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_s3_bucket.bck_ansible](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.bck_tfstate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_global_tags"></a> [global\_tags](#input\_global\_tags) | n/a | `map(string)` | <pre>{<br>  "Deployer": "LinkeIT",<br>  "Owner": "salvador.alavedra@syntax.com",<br>  "Project": "sap-ansible"<br>}</pre> | no |
| <a name="input_project_code"></a> [project\_code](#input\_project\_code) | Project code | `string` | `"lnk-demo"` | no |
| <a name="input_region"></a> [region](#input\_region) | n/a | `string` | `"eu-west-1"` | no |

## Outputs

No outputs.

## Authors

Module is maintained by [Salvador Alavedra](https://teams.microsoft.com/l/chat/0/0?users=salvador.alavedra@syntax.com) and [Javier Sahagun](https://teams.microsoft.com/l/chat/0/0?users=javier.sahagun@syntax.com).

## FAQ

#### Can I request new features?
See [Contributions](#Feedback)  section. Don't forget to check the [Change Log](CHANGELOG.md) before.
#### This module helps enforce best practices?
AWS published [SAP Lens](https://docs.aws.amazon.com/wellarchitected/latest/sap-lens/sap-lens.pdf) and this terraform module was created to help with some of the points listed there.
#### Rebuild from scratch is needed?
Existing AWS resources in your current AWS account(s) can be reused without downtime by this module via the ```terraform import
``` command. Be careful in your production environments, some settings can overwrite the existing ones.

## Feedback

Contributions are always welcome! Please read the [contribution guidelines](CONTRIBUTING.md) first

## Reporting a Vulnerability

If you find a security vulnerability affecting any of the supported roles, please email to authors following the [guidelines](CONTRIBUTING.md).

After receiving the initial report, we will endeavor to keep you informed of the progress towards a fix and full announcement.
We may ask you for additional information. You are also welcome to propose a patch or solution.

Report security bugs in third-party software to the person or team maintaining it.

## License

[Apache 2 Licensed](http://www.apache.org/licenses/LICENSE-2.0). See [LICENSE](LICENSE) for full details.

## Appendix

Any additional information goes [here](https://www.syntax.com)
<!-- END OF PRE-COMMIT-TERRAFORM DOCS -->